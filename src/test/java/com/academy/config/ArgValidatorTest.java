package com.academy.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ArgValidatorTest {

    private ArgValidator argValidator;

    @BeforeEach
    public void setUp() {
        argValidator = new ArgValidator();
    }

    @Test
    void testValidate() {
        String[] args = {"opt/data/input.txt", "opt/data/output.txt"};

        Assertions.assertTrue(argValidator.validate(args));

    }
}
