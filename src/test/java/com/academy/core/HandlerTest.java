package com.academy.core;

import com.academy.model.ArithmeticStatement;
import com.academy.model.Operation;
import com.academy.service.Calc;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class HandlerTest {
    private Calc calc;
    private Handler handler;

    @BeforeEach
    public void setUp() {
        calc = new Calc();
        handler = new Handler(calc);
    }

    @Test
    void testHandleAdd() {
        ArithmeticStatement arithmeticStatement = new ArithmeticStatement(Operation.ADD, 1, 2);
        int actualResult = handler.handle(arithmeticStatement);

        Assertions.assertEquals(3, actualResult);

    }
}
