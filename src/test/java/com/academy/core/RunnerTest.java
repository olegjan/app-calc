package com.academy.core;

import com.academy.model.ArithmeticStatement;
import com.academy.model.CalculatedStatement;
import com.academy.service.Calc;
import com.academy.service.Consumer;
import com.academy.service.Producer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static com.academy.model.Operation.ADD;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class RunnerTest {

    @Mock
    private Producer producer;

    @BeforeEach
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testRun() {
        ArithmeticStatement arithmeticStatement = new ArithmeticStatement(ADD, 1, 2);
        int result = 3;

        List<ArithmeticStatement> produced = new ArrayList<>();
        produced.add(arithmeticStatement);

        when(producer.produce()).thenReturn(produced);

        CalculatedStatement cs = new CalculatedStatement();
        cs.setArithmeticStatement(arithmeticStatement);
        cs.setResult(result);

        List<CalculatedStatement> actual = new ArrayList<>();
        actual.add(cs);

        Handler handler = new Handler(new Calc());
        Consumer consumer = (List<CalculatedStatement> statements) -> Assertions.assertEquals(actual, statements);

        Runner runner = new Runner(producer, consumer, handler);
        runner.run();

    }
}
