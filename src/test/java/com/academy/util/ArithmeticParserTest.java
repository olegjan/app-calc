package com.academy.util;

import com.academy.model.ArithmeticStatement;
import com.academy.model.Operation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

 class ArithmeticParserTest {

    @Test
    void testParse() {

        Assertions.assertEquals(new ArithmeticStatement(Operation.ADD, 1, 2), ArithmeticParser.parse("1+2"));
        Assertions.assertEquals(new ArithmeticStatement(Operation.MULT, 1, 2), ArithmeticParser.parse("1*2"));
        Assertions.assertEquals(new ArithmeticStatement(Operation.DIV, 1, 2), ArithmeticParser.parse("1/2"));
        Assertions.assertEquals(new ArithmeticStatement(Operation.SUB, 1, 2), ArithmeticParser.parse("1-2"));
        // Assertions.assertEquals(new ArithmeticStatement(Operation.ADD, 1, -2), ArithmeticParser.parse("1+-2"));
        // Assertions.assertEquals(new ArithmeticStatement(Operation.ADD, -1, 2), ArithmeticParser.parse("-1+2"));
        // Assertions.assertEquals(new ArithmeticStatement(Operation.MULT, 1, -2), ArithmeticParser.parse("1*-2"));
    }
}
