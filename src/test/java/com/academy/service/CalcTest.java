package com.academy.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

 class CalcTest {
    private Calc calc;

    @BeforeEach
    public void setUp() {
        calc = new Calc();
    }

    @Test
    void testAdd() {
        Assertions.assertEquals(3, calc.add(1, 2));
        Assertions.assertEquals(1, calc.add(-1, 2));
    }
}
