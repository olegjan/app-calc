package com.academy;

import com.academy.config.ArgValidator;
import com.academy.config.Cfg;
import com.academy.core.Handler;
import com.academy.core.Runner;
import com.academy.service.Calc;
import com.academy.service.Consumer;
import com.academy.service.Producer;
import com.academy.service.impl.ConsumerImpl;
import com.academy.service.impl.ProducerImpl;

import java.io.IOException;
import java.util.Arrays;

public class CalcApp {
    public static void main(String[] args) {
        System.out.println("app started");
        CalcApp app = new CalcApp();
        try {
            app.execute(args);
        } catch (Exception e) {
            System.out.println("Error. Details: " + e.getMessage());
        } finally {
            System.out.println("app completed");
        }
    }

    public void execute(String[] args) throws IOException {
        ArgValidator validator = new ArgValidator();
        if (!validator.validate(args)) {
            System.out.println("Wrong args: " + Arrays.toString(args));
            return;
        }

        Cfg cfg = new Cfg();
        cfg.load(args);

        Calc calc = new Calc();
        Handler handler = new Handler(calc);

        Consumer consumer = new ConsumerImpl(cfg.getOutput());
        Producer producer = new ProducerImpl(cfg.getInput());

        Runner runner = new Runner(producer, consumer, handler);
        runner.run();
    }
}
