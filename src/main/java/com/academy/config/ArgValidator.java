package com.academy.config;

public class ArgValidator {
    public boolean validate(String[] args) {
        return isSpecified(args);
    }

    public boolean isSpecified(String[] args) {
        return args != null && args.length >= 2;
    }

}
