package com.academy.config;

public class Cfg {
    private String input;
    private String output;

    public void load(String[] args) {
//        ensure(args);
        input = args[0];
        output = args[1];
    }

    public String getInput() {
        return input;
    }


    public String getOutput() {
        return output;
    }
}
