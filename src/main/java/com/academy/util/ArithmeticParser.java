package com.academy.util;

import com.academy.model.ArithmeticStatement;
import com.academy.model.CalculatedStatement;
import com.academy.model.Operation;

public class ArithmeticParser {
    private final static String FORMAT = "%d%s%d=%d";
    private final static String PLUS = "+";
    private final static String MINUS = "-";
    private final static String DIV = "/";
    private final static String MULT = "*";

    public static ArithmeticStatement parse(String statement) {
        Operation operation = detectOperation(statement);
        String[] args = statement.split("[+-/*]");

        return new ArithmeticStatement(operation, Integer.parseInt(args[0]), Integer.parseInt(args[1]));
    }

    public static String formatStatement(CalculatedStatement statement) {
        return String.format(FORMAT, statement.getArithmeticStatement().getFirst(),
                formatOperation(statement.getArithmeticStatement().getOperation()) ,
                statement.getArithmeticStatement().getSecond(),
                statement.getResult());
    }

    public static String formatOperation(Operation operation) {
        switch (operation) {
            case ADD:
                return "+";

            case SUB:
                return "-";

            case DIV:
                return "/";

            case MULT:
                return "*";

            default:
                throw new IllegalArgumentException(operation.name());
        }
    }

    private static Operation detectOperation(String statement) {
        if (statement.contains(PLUS))
            return Operation.ADD;

        if (statement.contains(MINUS))
            return Operation.SUB;

        if (statement.contains(DIV))
            return Operation.DIV;

        if (statement.contains(MULT))
            return Operation.MULT;

        throw new IllegalArgumentException(statement);
    }
}
