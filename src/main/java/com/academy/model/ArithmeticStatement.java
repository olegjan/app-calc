package com.academy.model;

import java.util.Objects;

public class ArithmeticStatement {
    private final Operation operation;
    private final int first;
    private final int second;

    public ArithmeticStatement(Operation operation, int first, int second) {
        this.operation = operation;
        this.first = first;
        this.second = second;
    }

    public Operation getOperation() {
        return operation;
    }

    public int getFirst() {
        return first;
    }

    public int getSecond() {
        return second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArithmeticStatement that = (ArithmeticStatement) o;
        return first == that.first && second == that.second && operation == that.operation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(operation, first, second);
    }

    @Override
    public String toString() {
        return "ArithmeticStatement{" +
                "operation=" + operation +
                ", first=" + first +
                ", second=" + second +
                '}';
    }
}
