package com.academy.model;

import java.util.Objects;

public class CalculatedStatement {
    private ArithmeticStatement arithmeticStatement;
    private int result;

    public ArithmeticStatement getArithmeticStatement() {
        return arithmeticStatement;
    }

    public void setArithmeticStatement(ArithmeticStatement arithmeticStatement) {
        this.arithmeticStatement = arithmeticStatement;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalculatedStatement that = (CalculatedStatement) o;
        return result == that.result && Objects.equals(arithmeticStatement, that.arithmeticStatement);
    }

    @Override
    public int hashCode() {
        return Objects.hash(arithmeticStatement, result);
    }

    @Override
    public String toString() {
        return "CalculatedStatement{" +
                "arithmeticStatement=" + arithmeticStatement +
                ", result=" + result +
                '}';
    }
}
