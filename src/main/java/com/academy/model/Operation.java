package com.academy.model;

public enum Operation {
    ADD,
    SUB,
    DIV,
    MULT
}
