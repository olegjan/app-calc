package com.academy.service;

import com.academy.model.CalculatedStatement;

import java.util.List;

public interface Consumer {
    void consume(List<CalculatedStatement> statements);
}
