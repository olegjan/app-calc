package com.academy.service.impl;

import com.academy.model.ArithmeticStatement;
import com.academy.service.Producer;
import com.academy.util.ArithmeticParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProducerImpl implements Producer {

    private final String path;

    public ProducerImpl(String path) {
        this.path = path;
    }

    @Override
    public List<ArithmeticStatement> produce() {
        List<ArithmeticStatement> statements = new ArrayList<>();

        try (FileReader fr = new FileReader(path);
             BufferedReader reader = new BufferedReader(fr)) {
            String statement;
            while ((statement = reader.readLine()) != null) {
                ArithmeticStatement arithmeticStatement = ArithmeticParser.parse(statement);
                statements.add(arithmeticStatement);
            }
        } catch (IOException e) {
            System.out.println("Error: " + e);
        }

        return statements;
    }
}
