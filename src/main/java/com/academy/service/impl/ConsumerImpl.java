package com.academy.service.impl;

import com.academy.core.Handler;
import com.academy.model.ArithmeticStatement;
import com.academy.model.CalculatedStatement;
import com.academy.service.Calc;
import com.academy.service.Consumer;
import com.academy.util.ArithmeticParser;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ConsumerImpl implements Consumer {

    private final String path;

    public ConsumerImpl(String path) {
        this.path = path;
    }

    @Override
    public void consume(List<CalculatedStatement> statements) {
        try (PrintWriter writer = new PrintWriter(path)) {
            for (CalculatedStatement st : statements) {
                writer.println(ArithmeticParser.formatStatement(st));
                System.out.println("handled statement: " + st);
            }

            writer.flush();
        } catch (IOException e) {
            System.out.println("Error. Details: " + e);
        }
    }


}
