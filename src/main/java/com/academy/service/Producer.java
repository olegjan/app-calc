package com.academy.service;

import com.academy.model.ArithmeticStatement;

import java.util.List;

public interface Producer {
    List<ArithmeticStatement> produce();
}
