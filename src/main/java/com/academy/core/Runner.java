package com.academy.core;

import com.academy.model.ArithmeticStatement;
import com.academy.model.CalculatedStatement;
import com.academy.service.Consumer;
import com.academy.service.Producer;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    private final Producer producer;
    private final Consumer consumer;
    private final Handler handler;

    public Runner(Producer producer, Consumer consumer, Handler handler) {
        this.producer = producer;
        this.consumer = consumer;
        this.handler = handler;
    }

    public void run() {
        List<ArithmeticStatement> produced = producer.produce();
        List<CalculatedStatement> handled = new ArrayList<>();

        for (ArithmeticStatement st : produced) {
            CalculatedStatement calculatedStatement = new CalculatedStatement();
            calculatedStatement.setArithmeticStatement(st);
            int value = handler.handle(st);
            calculatedStatement.setResult(value);
            handled.add(calculatedStatement);
        }
        consumer.consume(handled);
    }
}
