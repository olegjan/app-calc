package com.academy.core;

import com.academy.model.ArithmeticStatement;
import com.academy.service.Calc;

public class Handler {

    private final Calc calc;

    public Handler(Calc calc) {
        this.calc = calc;
    }

    public int handle(ArithmeticStatement arithmeticStatement) {
        switch (arithmeticStatement.getOperation()) {
            case ADD:
                return calc.add(arithmeticStatement.getFirst(), arithmeticStatement.getSecond());

            case SUB:
                return calc.sub(arithmeticStatement.getFirst(), arithmeticStatement.getSecond());

            case DIV:
                return calc.div(arithmeticStatement.getFirst(), arithmeticStatement.getSecond());

            case MULT:
                return calc.mult(arithmeticStatement.getFirst(), arithmeticStatement.getSecond());

            default:
                throw new UnsupportedOperationException("Illegal operation: " + arithmeticStatement.getOperation().name());
        }
    }
}
